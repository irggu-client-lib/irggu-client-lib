/****************************************************************************
**  irggu.h
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-Lib.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef IRGGU_H
#define IRGGU_H

#include <QString>

namespace IrGGu
{
    const float version = 1.0;
}

namespace IrGGu_Handshake
{
    const QString receive = "^IRGGU_PROTOCOL_VERSION\\s(?<version>[1-9][0-9]*\\.[0-9])$";
    const QString write   = "IRGGU_PROTOCOL_VERSION <version>";
}

namespace IrGGu_Close
{
    const QString write = "CLOSE";
}

namespace IrGGu_Acknowledgment_Client
{
    const QString receive = "^ACK_CLIENT\\s(?<value>[1-9][0-9]*)$";
    const QString write   = "ACK_CLIENT <value>";
}

namespace IrGGu_Acknowledgment_Server
{
    const QString receive = "^ACK_SERVER\\s(?<value>[1-9][0-9]*)$";
    const QString write   = "ACK_SERVER <value>";
}

namespace IrGGu_Login
{
    const int noError        = 0;
    const int wrongUserPass  = 1;
    const int maxClientLimit = 2;
}

namespace IrGGu_Alerts
{
    const int newPrivateMsg   = 1;
    const int newHighlightMsg = 2;
    const int newDccFile      = 3;
}

namespace IrGGu_Commands
{
    const QString login           = "LOGIN";
    const QString insertNetwork   = "INSERT_NETWORK";
    const QString insertChat      = "INSERT_CHAT";
    const QString insertNick      = "INSERT_NICK";
    const QString insertNicks     = "INSERT_NICKS";
    const QString removeNetwork   = "REMOVE_NETWORK";
    const QString removeChat      = "REMOVE_CHAT";
    const QString removeNick      = "REMOVE_NICK";
    const QString changeNick      = "CHANGE_NICK";
    const QString changeNickMode  = "CHANGE_NICK_MODE";
    const QString renameChat      = "RENAME_CHAT";
    const QString newMsg          = "NEW_MSG";
    const QString newHighlightMsg = "NEW_HIGHLIGHT_MSG";
    const QString newOwnMsg       = "NEW_OWN_MSG";
    const QString appendMsg       = "APPEND_MSG";
    const QString newDccFile      = "NEW_DCC_FILE";
    const QString closeDccFile    = "CLOSE_DCC_FILE";
    const QString lineCompleted   = "LINE_COMPLETED";
    const QString alert           = "ALERT";
    const QString closeAlert      = "CLOSE_ALERT";
    const QString regex           = "^(?<command>\\S+).*$";
}

namespace IrGGu_Receive
{
    const QString login           = "^LOGIN\\s(?<result>[0-2])(?:\\s(?<sessionID>[1-9][0-9]*))?(?:\\s(?<connectionID>[1-9][0-9]{6}))?$";
    const QString insertNetwork   = "^INSERT_NETWORK\\s(?<network>\\S+)$";
    const QString insertChat      = "^INSERT_CHAT\\s(?<network>\\S+)\\s(?<chat>\\S+)$";
    const QString insertNick      = "^INSERT_NICK\\s(?<network>\\S+)\\s(?<chat>\\S+)\\s(?<nick>\\S+)$";
    const QString insertNicks     = "^INSERT_NICKS\\s(?<network>\\S+)\\s(?<chat>\\S+)\\s(?<nicks>(?:\\s?\\S)+)$";
    const QString removeNetwork   = "^REMOVE_NETWORK\\s(?<network>\\S+)$";
    const QString removeChat      = "^REMOVE_CHAT\\s(?<network>\\S+)\\s(?<chat>\\S+)$";
    const QString removeNick      = "^REMOVE_NICK\\s(?<network>\\S+)\\s(?<chat>\\S+)\\s(?<nick>\\S+)$";
    const QString changeNick      = "^CHANGE_NICK\\s(?<network>\\S+)\\s(?<chat>\\S+)\\s(?<oldNick>\\S+)\\s(?<newNick>\\S+)$";
    const QString changeNickMode  = "^CHANGE_NICK_MODE\\s(?<network>\\S+)\\s(?<chat>\\S+)\\s(?<nick>\\S+)(?:\\s(?<mode>\\S))?$";
    const QString renameChat      = "^RENAME_CHAT\\s(?<network>\\S+)\\s(?<oldChat>\\S+)\\s(?<newChat>\\S+)$";
    const QString newMsg          = "^NEW_MSG\\s(?<network>\\S+)\\s(?<chat>\\S+)\\s(?<msg>.+)$";
    const QString newHighlightMsg = "^NEW_HIGHLIGHT_MSG\\s(?<network>\\S+)\\s(?<chat>\\S+)\\s(?<msg>.+)$";
    const QString newOwnMsg       = "^NEW_OWN_MSG\\s(?<network>\\S+)\\s(?<chat>\\S+)\\s(?<msg>.+)$";
    const QString appendMsg       = "^APPEND_MSG\\s(?<network>\\S+)\\s(?<chat>\\S+)\\s(?<msg>.+)$";
    const QString newDccFile      = "^NEW_DCC_FILE\\s(?<network>\\S+)\\s(?<sender>\\S+)\\s(?<file>.+)$";
    const QString closeDccFile    = "^CLOSE_DCC_FILE\\s(?<network>\\S+)\\s(?<sender>\\S+)\\s(?<file>.+)$";
    const QString lineCompleted   = "^LINE_COMPLETED\\s(?<network>\\S+)\\s(?<chat>\\S+)\\s(?<line>.+)$";
    const QString alert           = "^ALERT\\s(?<network>\\S+)\\s(?<sender>\\S+)\\s(?<type>[1-3])$";
    const QString closeAlert      = "^CLOSE_ALERT\\s(?<network>\\S+)\\s(?<sender>\\S+)\\s(?<type>[1-3])$";
}

namespace IrGGu_Write
{
    const QString login                 = "LOGIN <username> <password>";
    const QString loginSession          = "LOGIN <username> <password> <sessionID> <connectionID>";
    const QString startSession          = "START_SESSION";
    const QString startTemporarySession = "START_TEMPORARY_SESSION";
    const QString stopSession           = "STOP_SESSION";
    const QString newSettingValue       = "SETTING <category> <setting> <value>";
    const QString setColors             = "SET_COLORS <colors>";
    const QString setIdle               = "SET_IDLE <idle> <alerts> <priority>";
    const QString writeLine             = "WRITE_LINE <network> <chat> <line>";
    const QString completeLine          = "COMPLETE_LINE <network> <chat> <pos> <line>";
    const QString acceptDccFile         = "ACCEPT_DCC_FILE <network> <sender> <file>";
    const QString doNotAcceptDccFile    = "DO_NOT_ACCEPT_DCC_FILE <network> <sender> <file>";
    const QString quitNetwork           = "QUIT <network>";
    const QString closeChat             = "CLOSE_CHAT <network> <chat>";
    const QString query                 = "QUERY <network> <nick>";
}
#endif // IRGGU_H
