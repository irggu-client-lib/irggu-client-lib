/****************************************************************************
**  connection.cpp
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-Lib.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "protocol/irggu.h"
#include "connection.h"
#include <QRegularExpressionMatch>

/**
 * Constructor.
 *
 * @param *socket Socket of connection.
 * @param *login  Instance of Login-class.
 * @param *parent Pointer to parent.
 */
Connection::Connection (TcpSocket *socket, Login *login, QObject *parent) : QObject(parent)
{
    this->socket = socket;
    this->login  = login;

    connect(socket, SIGNAL(connected()), this, SLOT(connected()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));

    this->commands.insert(IrGGu_Commands::insertNetwork, Commands::InsertNetwork);
    this->commands.insert(IrGGu_Commands::insertChat, Commands::InsertChat);
    this->commands.insert(IrGGu_Commands::insertNick, Commands::InsertNick);
    this->commands.insert(IrGGu_Commands::insertNicks, Commands::InsertNicks);
    this->commands.insert(IrGGu_Commands::removeNetwork, Commands::RemoveNetwork);
    this->commands.insert(IrGGu_Commands::removeChat, Commands::RemoveChat);
    this->commands.insert(IrGGu_Commands::removeNick, Commands::RemoveNick);
    this->commands.insert(IrGGu_Commands::changeNick, Commands::ChangeNick);
    this->commands.insert(IrGGu_Commands::changeNickMode, Commands::ChangeNickMode);
    this->commands.insert(IrGGu_Commands::renameChat, Commands::RenameChat);
    this->commands.insert(IrGGu_Commands::newMsg, Commands::NewMsg);
    this->commands.insert(IrGGu_Commands::newHighlightMsg, Commands::NewHighlightMsg);
    this->commands.insert(IrGGu_Commands::newOwnMsg, Commands::NewOwnMsg);
    this->commands.insert(IrGGu_Commands::appendMsg, Commands::AppendMsg);
    this->commands.insert(IrGGu_Commands::newDccFile, Commands::NewDCCFile);
    this->commands.insert(IrGGu_Commands::closeDccFile, Commands::CloseDCCFile);
    this->commands.insert(IrGGu_Commands::lineCompleted, Commands::LineCompleted);
    this->commands.insert(IrGGu_Commands::alert, Commands::Alert);
    this->commands.insert(IrGGu_Commands::closeAlert, Commands::CloseAlert);
}

/**
 * Connects to server.
 */
void Connection::connectToServer ()
{
    prevBuffer        = buffer;
    wroteSessionStart = false;
    wroteSessionStop  = false;

    buffer.clear();

    this->login->connectToServer();
}

/**
 * Send colors to server.
 *
 * @param colorsList List of colors.
 */
void Connection::setColors (QMap<QString, QString> colorsList)
{
    QString command = IrGGu_Write::setColors;
    QString colors;

    colors.append(" " + colorsList.value("foreground"));
    colors.append(" " + colorsList.value("local0"));
    colors.append(" " + colorsList.value("local1"));
    colors.append(" " + colorsList.value("local2"));
    colors.append(" " + colorsList.value("local3"));
    colors.append(" " + colorsList.value("local4"));
    colors.append(" " + colorsList.value("local5"));
    colors.append(" " + colorsList.value("local6"));
    colors.append(" " + colorsList.value("local7"));
    colors.append(" " + colorsList.value("local8"));
    colors.append(" " + colorsList.value("local9"));
    colors.append(" " + colorsList.value("local10"));
    colors.append(" " + colorsList.value("local11"));
    colors.append(" " + colorsList.value("local12"));
    colors.append(" " + colorsList.value("local13"));
    colors.append(" " + colorsList.value("local14"));
    colors.append(" " + colorsList.value("local15"));
    colors.append(" " + colorsList.value("mirc0"));
    colors.append(" " + colorsList.value("mirc1"));
    colors.append(" " + colorsList.value("mirc2"));
    colors.append(" " + colorsList.value("mirc3"));
    colors.append(" " + colorsList.value("mirc4"));
    colors.append(" " + colorsList.value("mirc5"));
    colors.append(" " + colorsList.value("mirc6"));
    colors.append(" " + colorsList.value("mirc7"));
    colors.append(" " + colorsList.value("mirc8"));
    colors.append(" " + colorsList.value("mirc9"));
    colors.append(" " + colorsList.value("mirc10"));
    colors.append(" " + colorsList.value("mirc11"));
    colors.append(" " + colorsList.value("mirc12"));
    colors.append(" " + colorsList.value("mirc13"));
    colors.append(" " + colorsList.value("mirc14"));
    colors.append(" " + colorsList.value("mirc15"));

    command.replace("<colors>", colors);

    write(command);
}

/**
 * Writes start session command to server.
 *
 * @param temporary Specifies if the session is temporary.
 */
void Connection::startSession (bool temporary)
{
    connect(socket, SIGNAL(readyRead()), this, SLOT(readData()));
    connect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWritten(qint64)));

    if ( temporary )
    {
        write(IrGGu_Write::startTemporarySession);
    }
    else
    {
        write(IrGGu_Write::startSession);
    }

    buffer.append(prevBuffer);
    prevBuffer.clear();

    wroteSessionStart = true;

    writeBuffer();
}

/**
 * Writes stop session command to server.
 */
void Connection::stopSession ()
{
    write(IrGGu_Write::stopSession);

    wroteSessionStop = true;
}

/**
 * Writes write line command to server.
 *
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Line to write into IRC.
 */
void Connection::writeLine(QString network, QString chat, QString line)
{
    QString command = IrGGu_Write::writeLine;

    command.replace("<network>", network);
    command.replace("<chat>", chat);
    command.replace("<line>", line);

    write(command);
}

/**
 * Writes complete line command to server.
 *
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Line to complete.
 * @param pos     Cursors position.
 */
void Connection::completeLine(QString network, QString chat, QString line, int pos)
{
    QString command = IrGGu_Write::completeLine;

    int last = line.length() - 1;

    if ( line.at(last) == ' ' )
    {
        line.replace(last, 1, "<space>");
    }

    command.replace("<network>", network);
    command.replace("<chat>", chat);
    command.replace("<pos>", QString::number(pos));
    command.replace("<line>", line);

    write(command);
}

/**
 * Writes write setting command to server.
 *
 * @param category Setting category.
 * @param setting  Setting name.
 * @param value    Setting value.
 */
void Connection::writeSetting(QString category, QString setting, QString value)
{
    QString command = IrGGu_Write::newSettingValue;

    command.replace("<category>", category);
    command.replace("<setting>", setting);
    command.replace("<value>", value);

    write(command);
}

/**
 * Writes set idle command to server.
 *
 * @param idle     Specifies idle state.
 * @param alerts   Specifies alerts state.
 * @param priority Priority of client.
 */
void Connection::setIdle(bool idle, bool alerts, int priority)
{
    QString command = IrGGu_Write::setIdle;

    command.replace("<idle>", idle ? "true" : "false");
    command.replace("<alerts>", alerts ? "true" : "false");
    command.replace("<priority>", QString::number(priority));

    write(command);
}

/**
 * Writes DCC file reply command to server.
 *
 * @param network Network name.
 * @param sender  Sender's name.
 * @param file    Filename.
 * @param get     Specifies if the file should be accepted.
 */
void Connection::writeDccFileReply(QString network, QString sender, QString file, bool get)
{
    QString command;

    if ( get )
    {
        command = IrGGu_Write::acceptDccFile;
    }
    else
    {
        command = IrGGu_Write::doNotAcceptDccFile;
    }

    command.replace("<network>", network);
    command.replace("<sender>", sender);
    command.replace("<file>", file);

    write(command);
}

/**
 * Writes quit network command to server.
 *
 * @param network Network name.
 */
void Connection::quitNetwork(QString network)
{
    QString command = IrGGu_Write::quitNetwork;

    command.replace("<network>", network);

    write(command);
}

/**
 * Writes close chat command to server.
 *
 * @param network Network name.
 * @param chat    Chat name.
 */
void Connection::closeChat(QString network, QString chat)
{
    QString command = IrGGu_Write::closeChat;

    command.replace("<network>", network);
    command.replace("<chat>", chat);

    write(command);
}

/**
 * Writes query command to server.
 *
 * @param network Network name.
 * @param nick    User's nick.
 */
void Connection::query(QString network, QString nick)
{
    QString command = IrGGu_Write::query;

    command.replace("<network>", network);
    command.replace("<nick>", nick);

    write(command);
}

/**
 * Writes line to server.
 *
 * @param line Line to be written.
 * @param last Specifies that should the line to be added as last in buffer.
 */
void Connection::write (QString line)
{
    line += "\n";

    BufferedLine *bufferedLine = new BufferedLine();

    bufferedLine->bytes = socket->write(line.toUtf8());
    bufferedLine->line  = line;
    bufferedLine->write = wroteSessionStart;

    buffer.append(bufferedLine);
}

/**
 * Write line buffer to server.
 */
void Connection::writeBuffer ()
{
    QMutableListIterator<BufferedLine *> bufferIt(buffer);
    BufferedLine                         *bufferedLine;

    while ( bufferIt.hasNext() )
    {
        bufferedLine = bufferIt.next();

        if ( bufferedLine->write )
        {
            socket->write(bufferedLine->line.toUtf8());
        }
    }
}

/**
 * Handles commands received from the server.
 *
 * @param line Command received from the server.
 */
void Connection::handleCommand (QString line)
{
    QRegularExpression commandRe(IrGGu_Commands::regex);
    QRegularExpressionMatch commandRem = commandRe.match(line);

    switch ( commands.value(commandRem.captured("command")) )
    {
        case Commands::InsertNetwork:
        {
            QRegularExpression re(IrGGu_Receive::insertNetwork);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT insertNetwork(rem.captured("network"));
            }

            break;
        }

        case Commands::InsertChat:
        {
            QRegularExpression re(IrGGu_Receive::insertChat);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT insertChat(rem.captured("network"), rem.captured("chat"));
            }

            break;
        }

        case Commands::InsertNick:
        {
            QRegularExpression re(IrGGu_Receive::insertNick);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT insertNick(rem.captured("network"), rem.captured("chat"), rem.captured("nick"));
            }

            break;
        }

        case Commands::InsertNicks:
        {
            QRegularExpression re(IrGGu_Receive::insertNicks);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT insertNicks(rem.captured("network"), rem.captured("chat"), rem.captured("nicks").split(' '));
            }

            break;
        }

        case Commands::RemoveNetwork:
        {
            QRegularExpression re(IrGGu_Receive::removeNetwork);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT removeNetwork(rem.captured("network"));
            }

            break;
        }

        case Commands::RemoveChat:
        {
            QRegularExpression re(IrGGu_Receive::removeChat);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT removeChat(rem.captured("network"), rem.captured("chat"));
            }

            break;
        }

        case Commands::RemoveNick:
        {
            QRegularExpression re(IrGGu_Receive::removeNick);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT removeNick(rem.captured("network"), rem.captured("chat"), rem.captured("nick"));
            }

            break;
        }

        case Commands::ChangeNick:
        {
            QRegularExpression re(IrGGu_Receive::changeNick);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT changeNick(rem.captured("network"), rem.captured("chat"), rem.captured("oldNick"), rem.captured("newNick"));
            }

            break;
        }

        case Commands::ChangeNickMode:
        {
            QRegularExpression re(IrGGu_Receive::changeNickMode);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT changeNickMode(rem.captured("network"), rem.captured("chat"), rem.captured("nick"), rem.captured("mode"));
            }

            break;
        }

        case Commands::RenameChat:
        {
            QRegularExpression re(IrGGu_Receive::renameChat);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT renameChat(rem.captured("network"), rem.captured("oldChat"), rem.captured("newChat"));
            }

            break;
        }

        case Commands::NewMsg:
        {
            QRegularExpression re(IrGGu_Receive::newMsg);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT newMsg(rem.captured("network"), rem.captured("chat"), parseMsgLine(rem.captured("msg")));
            }

            break;
        }

        case Commands::NewHighlightMsg:
        {
            QRegularExpression re(IrGGu_Receive::newHighlightMsg);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT newHighlightMsg(rem.captured("network"), rem.captured("chat"), parseMsgLine(rem.captured("msg")));
            }

            break;
        }

        case Commands::NewOwnMsg:
        {
            QRegularExpression re(IrGGu_Receive::newOwnMsg);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT newOwnMsg(rem.captured("network"), rem.captured("chat"), parseMsgLine(rem.captured("msg")));
            }

            break;
        }

        case Commands::AppendMsg:
        {
            QRegularExpression re(IrGGu_Receive::appendMsg);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT appendMsg(rem.captured("network"), rem.captured("chat"), parseMsgLine(rem.captured("msg")));
            }

            break;
        }

        case Commands::NewDCCFile:
        {
            QRegularExpression re(IrGGu_Receive::newDccFile);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT newDCCFile(rem.captured("network"), rem.captured("sender"), rem.captured("file"));
            }

            break;
        }

        case Commands::CloseDCCFile:
        {
            QRegularExpression re(IrGGu_Receive::closeDccFile);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT closeDCCFile(rem.captured("network"), rem.captured("sender"), rem.captured("file"));
            }

            break;
        }

        case Commands::LineCompleted:
        {
            QRegularExpression re(IrGGu_Receive::lineCompleted);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT lineCompleted(rem.captured("network"), rem.captured("chat"), rem.captured("line").replace("<space>", " "));
            }

            break;
        }

        case Commands::Alert:
        {
            QRegularExpression re(IrGGu_Receive::alert);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT showAlert(rem.captured("network"), rem.captured("sender"), rem.captured("type").toInt());
            }

            break;
        }

        case Commands::CloseAlert:
        {
            QRegularExpression re(IrGGu_Receive::closeAlert);
            QRegularExpressionMatch rem = re.match(line);

            if ( rem.hasMatch() )
            {
                Q_EMIT closeAlert(rem.captured("network"), rem.captured("sender"), rem.captured("type").toInt());
            }

            break;
        }
    }
}

/**
 * Parses the message line.
 *
 * @param text Message line.
 * @return Parsed line.
 */
Line Connection::parseMsgLine(QString text)
{
    Line line;

    QRegularExpression re("<line><timestamp>(?<timestamp>.+?)</timestamp><nick>(?<nick>.+?)</nick>(?<msg>.+?)</line>");
    QRegularExpressionMatch rem = re.match(text);

    line.insert("timestamp", rem.captured("timestamp"));
    line.insert("nick", rem.captured("nick"));
    line.insert("msg", rem.captured("msg"));

    return line;
}

/**
 * Called when client has connected to server.
 */
void Connection::connected ()
{
    login->login();
}

/**
 * Called when client has disconnected from the server.
 */
void Connection::disconnected ()
{
    disconnect(socket, SIGNAL(readyRead()), this, SLOT(readData()));
    disconnect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWritten(qint64)));
}

/**
 * Called when there is data to read.
 */
void Connection::readData ()
{
    QString line;

    while ( socket->canReadLine() )
    {
        line = QString::fromUtf8(socket->readLine().trimmed());

        handleCommand(line);
    }
}

/**
 * Called when data has been sent to client.
 *
 * @param bytes Number of bytes written.
 */
void Connection::bytesWritten (qint64 bytes)
{
    if ( !buffer.isEmpty() )
    {
        BufferedLine *bufferedLine = buffer.takeFirst();

        if ( bufferedLine->bytes == bytes )
        {
            delete bufferedLine;
        }

        if ( wroteSessionStop )
        {
            Q_EMIT sessionStopped();
        }
    }
}

/**
 * Signal is emitted when server has received stop session command.
 *
 * @fn void Connection::sessionStopped()
 */

/**
 * Signal is emitted when insert network command has been received.
 *
 * @fn void Connection::insertNetwork(QString network)
 * @param network Network name.
 */

/**
 * Signal is emitted when insert chat command has been received.
 *
 * @fn void Connection::insertChat(QString network, QString chat)
 * @param network Network name.
 * @param chat    Chat name.
 */

/**
 * Signal is emitted when insert nick command has been received.
 *
 * @fn void Connection::insertNick(QString network, QString chat, QString nick)
 * @param network Network name.
 * @param chat    Chat name.
 * @param nick    Nick.
 */

/**
 * Signal is emitted when insert nicks command has been received.
 *
 * @fn void Connection::insertNicks(QString network, QString chat, QStringList nicks)
 * @param network Network name.
 * @param chat    Chat name.
 * @param nicks   Nicks.
 */

/**
 * Signal is emitted when remove network command has been received.
 *
 * @fn void Connection::removeNetwork(QString network)
 * @param network Network name.
 */

/**
 * Signal is emitted when remove chat command has been received.
 *
 * @fn void Connection::removeChat(QString network, QString chat)
 * @param network Network name.
 * @param chat    Chat name.
 */

/**
 * Signal is emitted when remove nick command has been received.
 *
 * @fn void Connection::removeNick(QString network, QString chat, QString nick)
 * @param network Network name.
 * @param chat    Chat name.
 * @param nick    Nick.
 */


/**
 * Signal is emitted when change nick command has been received.
 *
 * @fn void Connection::changeNick(QString network, QString chat, QString oldNick, QString newNick)
 * @param network Network name.
 * @param chat    Chat name.
 * @param oldNick Old nick.
 * @param newNick New nick.
 */

/**
 * Signal is emitted when change nick mode command has been received.
 *
 * @fn void Connection::changeNickMode(QString network, QString chat, QString nick, QString mode)
 * @param network Network name.
 * @param chat    Chat name.
 * @param nick    Nick.
 * @param mode    New mode.
 */

/**
 * Signal is emitted when rename chat command has been received.
 *
 * @fn void Connection::renameChat(QString network, QString oldChat, QString newChat)
 * @param network Network name.
 * @param oldChat Old chat name.
 * @param newChat New chat name.
 */

/**
 * Signal is emitted when new message command has been received.
 *
 * @fn void Connection::newMsg(QString network, QString chat, Line line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Chat line.
 */

/**
 * Signal is emitted when new highlight message command has been received.
 *
 * @fn void Connection::newHighlightMsg(QString network, QString chat, Line line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Chat line.
 */


/**
 * Signal is emitted when new own message command has been received.
 *
 * @fn void Connection::newOwnMsg(QString network, QString chat, Line line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Chat line.
 */

/**
 * Signal is emitted when append message command has been received.
 *
 * @fn void Connection::appendMsg(QString network, QString chat, Line line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Chat line.
 */

/**
 * Signal is emitted when new DCC file command has been received.
 *
 * @fn void Connection::newDCCFile(QString network, QString sender, QString file)
 * @param network Network name.
 * @param sender  Sender's name.
 * @param file    Filename.
 */

/**
 * Signal is emitted when close DCC file command has been received.
 *
 * @fn void Connection::closeDCCFile(QString network, QString sender, QString file)
 * @param network Network name.
 * @param sender  Sender's name.
 * @param file    Filename.
 */

/**
 * Signal is emitted when line completed command has been received.
 *
 * @fn void Connection::lineCompleted(QString network, QString chat, QString line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Completed line.
 */

/**
 * Signal is emitted when alert command has been received.
 *
 * @fn void Connection::showAlert(QString network, QString sender, int type)
 * @param network Network name.
 * @param sender  Triggerer of alert.
 * @param type    Alert type.
 */

/**
 * Signal is emitted when close alert command has been received.
 *
 * @fn void Connection::closeAlert(QString network, QString sender, int type)
 * @param network Network name.
 * @param sender  Triggerer of alert.
 * @param type    Alert type.
 */
