/****************************************************************************
**  tcpsocket.cpp
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-Lib.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "protocol/irggu.h"
#include "tcpsocket.h"
#include <QRegularExpression>

/**
 * Constructor.
 *
 * @param *parent Pointer to parent.
 */
TcpSocket::TcpSocket (QObject *parent) : QObject(parent)
{
    socket   = new QSslSocket(this);
    timeout  = new QTimer(this);
    interval = 300000;

    connect(socket, SIGNAL(disconnected()), this, SIGNAL(disconnected()));
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error()));
    connect(socket, SIGNAL(sslErrors(QList<QSslError>)), this,
            SIGNAL(sslErrors(QList<QSslError>)));
    connect(socket, SIGNAL(readyRead()), this, SLOT(readData()));
    connect(timeout, SIGNAL(timeout()), this, SLOT(disconnectImmediately()));
}

/**
 * Connects to host.
 *
 * @param hostName Hostname.
 * @param port     Port.
 */
void TcpSocket::connectToHost(const QString & hostName, quint16 port)
{
    receivedHandshake = false;

    receivedACK.clear();
    writtenACK.clear();

    socket->connectToHost(hostName, port);

    connect(socket, SIGNAL(connected()), this, SLOT(sendHandshake()));
}

/**
 * Connects to host with encryption.
 *
 * @param hostName Hostname.
 * @param port     Port.
 */
void TcpSocket::connectToHostEncrypted(const QString & hostName, quint16 port)
{
    receivedHandshake = false;

    receivedACK.clear();
    writtenACK.clear();

    socket->connectToHostEncrypted(hostName, port);

    connect(socket, SIGNAL(encrypted()), this, SLOT(sendHandshake()));
}

/**
 * Disconnects from host.
 */
void TcpSocket::disconnectFromHost ()
{
    QString command = IrGGu_Close::write + "\n";

    socket->write(command.toUtf8());
}

/**
 * Check if line can be read.
 *
 * @return True if line can be read.
 */
bool TcpSocket::canReadLine ()
{
    bool canReadLine = false;

    if ( receivedHandshake && !buffer.isEmpty() )
    {
        canReadLine = true;
    }

    return canReadLine;
}

/**
 * Reads a line.
 *
 * @return Line that was read.
 */
QByteArray TcpSocket::readLine ()
{
    QByteArray line;

    if ( receivedHandshake )
    {
        line = buffer.takeFirst();
    }

    return line;
}

/**
 * Writes a data.
 *
 * @param data Data that is going to be written.
 * @return Bytes written.
 */
qint64 TcpSocket::write (QByteArray data)
{
    qint64 bytes = 0;

    if ( receivedHandshake )
    {
        bytes = data.size();

        QString ack = IrGGu_Acknowledgment_Client::write + "\n";

        ack.replace("<value>", QString::number(bytes));

        writtenACK.append(bytes);

        socket->write(ack.toUtf8());
        socket->write(data);

        timeout->start(interval);
    }

    return bytes;
}

/**
 * Handles IrGGu-protocol handshake.
 *
 * @param line Line from the socket.
 */
void TcpSocket::handleHandshake(QString line)
{
    QRegularExpression re(IrGGu_Handshake::receive);
    QRegularExpressionMatch rem = re.match(line);

    if ( rem.hasMatch() )
    {
        if ( rem.captured("version").toFloat() == IrGGu::version )
        {
            receivedHandshake = true;

            Q_EMIT connected();
        }
    }
}

/**
 * Write ACK reply.
 */
void TcpSocket::writeACKReply ()
{
    if ( !receivedACK.isEmpty() )
    {
        QString ack = IrGGu_Acknowledgment_Server::write + "\n";

        ack.replace("<value>", receivedACK.takeFirst());

        socket->write(ack.toUtf8());
    }
}

/**
 * Check if line is ACK message.
 *
 * @param line Line from client.
 * @return True if line is ACK message.
 */
bool TcpSocket::checkACK (QByteArray line)
{
    bool    ack        = false;
    QString ackMessage = QString::fromUtf8(line).trimmed();

    QRegularExpression      reC(IrGGu_Acknowledgment_Client::receive);
    QRegularExpressionMatch remC = reC.match(ackMessage);
    QRegularExpression      reS(IrGGu_Acknowledgment_Server::receive);
    QRegularExpressionMatch remS = reS.match(ackMessage);

    if ( remC.hasMatch() )
    {
        ack = true;

        qint64 bytes = writtenACK.takeFirst();

        if ( bytes == remC.captured("value").toLongLong() )
        {
            timeout->stop();

            Q_EMIT bytesWritten(bytes);
        }
        else
        {
            disconnectImmediately();
        }
    }
    else if ( remS.hasMatch() )
    {
        ack = true;

        receivedACK.append(remS.captured("value"));
    }

    return ack;
}

/**
 * Ignores SSL errors.
 */
void TcpSocket::ignoreSslErrors()
{
    socket->ignoreSslErrors();
}

/**
 * Disconnects from server immediately.
 */
void TcpSocket::disconnectImmediately()
{
    socket->disconnectFromHost();
}

/**
 * Read data sent by server.
 */
void TcpSocket::readData ()
{
    while ( socket->canReadLine() )
    {
        if ( !receivedHandshake )
        {
            QString line;

            line = QString::fromUtf8(socket->readLine()).trimmed();

            handleHandshake(line);
        }
        else
        {
            QByteArray data = socket->readLine();
            bool       ack  = checkACK(data);

            if ( !ack && !receivedACK.isEmpty() )
            {
                buffer.append(data);

                writeACKReply();

                Q_EMIT readyRead();
            }
            else if ( !ack )
            {
                disconnectImmediately();
            }
        }
    }
}

/**
 * Sends handshake to server.
 */
void TcpSocket::sendHandshake()
{
    if ( socket->isEncrypted() )
    {
        disconnect(socket, SIGNAL(encrypted()), this, SLOT(sendHandshake()));
    }
    else
    {
        disconnect(socket, SIGNAL(connected()), this, SLOT(sendHandshake()));
    }

    QString version = QString::number(IrGGu::version, 10, 1);

    QString line = IrGGu_Handshake::write + "\n";

    line.replace("<version>", version);

    socket->write(line.toUtf8());
}

/**
 * Called when there is error with connection.
 */
void TcpSocket::error ()
{
    if ( !receivedHandshake )
    {
        Q_EMIT unableToConnect();
    }
}

/**
 * Signal is emitted when socket has connected to server.
 *
 * @fn void TcpSocket::connected()
 */

/**
 * Signal is emitted when socket has disconnected from server.
 *
 * @fn void TcpSocket::disconnected()
 */

/**
 * Signal is emitted when socket was unable to connect.
 *
 * @fn void TcpSocket::unableToConnect()
 */

/**
 * Signal is emitted when there is SSL errors.
 *
 * @fn void TcpSocket::sslErrors(QList<QSslError> errors)
 * @param errors SSL errors.
 */

/**
 * Signal is emitted when socket has data for reading.
 *
 * @fn void TcpSocket::readyRead()
 */

/**
 * Signal is emitted when socket wrote data.
 *
 * @fn void TcpSocket::bytesWritten(qint64 bytes)
 * @param bytes Number of bytes written.
 */
