/****************************************************************************
**  tcpsocket.h
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-Lib.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include <QObject>
#include <QSslSocket>
#include <QTimer>

/**
 *  Wrapper to QSslSocket that handles IrGGu-protocol handshake.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2015-02-07
 */
class TcpSocket : public QObject
{
    Q_OBJECT
public:
    explicit TcpSocket(QObject *parent = 0);
    
    void       connectToHost(const QString & hostName, quint16 port);
    void       connectToHostEncrypted(const QString & hostName, quint16 port);
    void       disconnectFromHost();
    bool       canReadLine();
    QByteArray readLine();
    qint64     write(QByteArray data);

private:
    QSslSocket        *socket;
    bool              receivedHandshake;
    QStringList       receivedACK;
    QList<qint64>     writtenACK;
    QList<QByteArray> buffer;
    QTimer            *timeout;
    int               interval;

    void handleHandshake(QString line);
    void writeACKReply();
    bool checkACK(QByteArray line);

public Q_SLOTS:
    void ignoreSslErrors();

private Q_SLOTS:
    void disconnectImmediately();
    void readData();
    void sendHandshake();
    void error();

Q_SIGNALS:
    void connected();
    void disconnected();
    void unableToConnect();
    void sslErrors(QList<QSslError> errors);
    void readyRead();
    void bytesWritten(qint64 bytes);
};

#endif // TCPSOCKET_H
