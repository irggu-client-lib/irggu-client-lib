/****************************************************************************
**  login.cpp
**
**  Copyright information
**
**      Copyright (C) 2013-2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-Lib.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "protocol/irggu.h"
#include "login.h"
#include <QRegularExpression>

/**
 * Constructor.
 *
 * @param *socket         Socket of connection.
 * @param host            Host.
 * @param username        Username.
 * @param password        Password.
 * @param port            Port.
 * @param useSsl          Specifies if SSL is used.
 * @param ignoreSslErrors Specifies if SSL errors are ignored.
 * @param sessionID       Session ID of existing session.
 * @param connectionID    Connection ID of existing session.
 * @param *parent         Pointer to parent.
 */
Login::Login (TcpSocket *socket, QString host, QString username, QString password, int port,
              bool useSsl, bool ignoreSslErrors, int sessionID, int connectionID,
              QObject *parent) : QObject(parent)
{
    this->socket          = socket;
    this->host            = host;
    this->username        = username;
    this->password        = password;
    this->port            = port;
    this->sessionID       = sessionID;
    this->connectionID    = connectionID;
    this->useSsl          = useSsl;
    this->ignoreSslErrors = ignoreSslErrors;
    this->oldSession      = false;
}

/**
 * Check if the session is old session.
 *
 * @return True if session existed before login.
 */
bool Login::isOldSession ()
{
    return oldSession;
}

/**
 * Connects to server.
 */
void Login::connectToServer ()
{
    loginResult = -1;
    connect(socket, SIGNAL(readyRead()), this, SLOT(readData()));
    connect(socket, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(sslErrors()));

    if ( useSsl )
    {
        socket->connectToHostEncrypted(host, port);
    }
    else
    {
        socket->connectToHost(host, port);
    }
}

/**
 * Logs in to server.
 */
void Login::login ()
{
    QString command = IrGGu_Write::login;

    if ( sessionID > 0 && connectionID > 0 )
    {
        command = IrGGu_Write::loginSession;

        command.replace("<sessionID>", QString::number(sessionID));
        command.replace("<connectionID>", QString::number(connectionID));
    }

    command.replace("<username>", username);
    command.replace("<password>", password);

    write(command);
}

/**
 * Writes line to server.
 *
 * @param line Line to be written.
 */
void Login::write (QString line)
{
    line += "\n";
    socket->write(line.toUtf8());
}


/**
 * Called when there is data to read.
 */
void Login::readData ()
{  
    disconnect(socket, SIGNAL(readyRead()), this, SLOT(readData()));
    disconnect(this->socket, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(sslErrors()));
    QString line;

    while ( socket->canReadLine() )
    {
        line = QString::fromUtf8(socket->readLine().trimmed());

        QRegularExpression re(IrGGu_Receive::login);
        QRegularExpressionMatch rem = re.match(line);

        if ( rem.hasMatch() )
        {
            loginResult = rem.captured("result").toInt();

            if ( loginResult == 0 )
            {
                int newSessionID    = rem.captured("sessionID").toInt();
                int newConnectionID = rem.captured("connectionID").toInt();

                if ( newSessionID == sessionID && newConnectionID == connectionID )
                {
                    oldSession = true;
                }

                sessionID    = newSessionID;
                connectionID = newConnectionID;
            }
        }
    }

    Q_EMIT resultOfLogin(loginResult, sessionID, connectionID);
}

/**
 * Called when there is SSL errors.
 */
void Login::sslErrors ()
{
    if ( ignoreSslErrors )
    {
        socket->ignoreSslErrors();
    }
}

/**
 * Signal is emitted when login result has been received.
 *
 * @fn void Login::resultOfLogin(int result, int sessionID, int connectionID)
 * @param result       Login Result.
 * @param sessionID    Session ID.
 * @param connectionID Connection ID.
 */
