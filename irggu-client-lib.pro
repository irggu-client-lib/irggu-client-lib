#-------------------------------------------------
#
# Project created by QtCreator 2013-12-21T22:50:12
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = irggu-client-lib
TEMPLATE = lib

SOURCES += irgguclient.cpp \
    login.cpp \
    network/tcpsocket.cpp \
    connection.cpp

HEADERS += irgguclient.h \
    login.h \
    network/tcpsocket.h \
    protocol/irggu.h \
    connection.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

QMAKE_CXXFLAGS += -std=c++11

DISTFILES += \
    CMakeLists.txt
