/****************************************************************************
**  login.h
**
**  Copyright information
**
**      Copyright (C) 2013-2014 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-Lib.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef LOGIN_H
#define LOGIN_H

#include "network/tcpsocket.h"
#include <QObject>

/**
 *  This class is for handling login to IrGGu-Server.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2014-07-05
 */
class Login : public QObject
{
    Q_OBJECT
public:
    Login(TcpSocket *socket, QString host, QString username, QString password, int port,
          bool useSsl, bool ignoreSslErrors, int sessionID, int connectionID, QObject *parent = 0);

    bool    isOldSession();
    void    connectToServer();
    void    login();

private:
    int        loginResult;
    TcpSocket  *socket;
    QString    host;
    QString    username;
    QString    password;
    int        port;
    int        sessionID;
    int        connectionID;
    bool       useSsl;
    bool       ignoreSslErrors;
    bool       oldSession;

    void write(QString line);

Q_SIGNALS:
    void resultOfLogin(int result, int sessionID, int connectionID);

private Q_SLOTS:
    void readData();
    void sslErrors();

};

#endif // LOGIN_H
