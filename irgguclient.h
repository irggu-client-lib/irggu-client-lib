/****************************************************************************
**  irgguclient.h
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-Lib.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef IRGGUCLIENT_H
#define IRGGUCLIENT_H

#include "network/tcpsocket.h"
#include "login.h"
#include "connection.h"
#include <QObject>

/**
 *  This class is the base class for GUI main class.
 *
 *  @author  Arttu Liimola <arttu.liimola@gmail.com>
 *  @version 2015-02-01
 */
class IrGGuClient : public QObject
{
   Q_OBJECT
public:
    explicit IrGGuClient(QObject *parent = 0);

private:
    TcpSocket  *socket;
    Login      *login;
    Connection *connection;

    void createConnection();

Q_SIGNALS:
    void connected();
    void disconnected();
    void unableToConnect();
    void loginResult(int result, int sessionID, int connectionID);
    void insertNetwork(QString network);
    void insertChat(QString network, QString chat);
    void insertNick(QString network, QString chat, QString nick);
    void insertNicks(QString network, QString chat, QStringList nicks);
    void removeNetwork(QString network);
    void removeChat(QString network, QString chat);
    void removeNick(QString network, QString chat, QString nick);
    void changeNick(QString network, QString chat, QString oldNick, QString newNick);
    void changeNickMode(QString network, QString chat, QString nick, QString mode);
    void renameChat(QString network, QString oldChat, QString newChat);
    void newMsg(QString network, QString chat, Line line);
    void newHighlightMsg(QString network, QString chat, Line line);
    void newOwnMsg(QString network, QString chat, Line line);
    void appendMsg(QString network, QString chat, Line line);
    void newDCCFile(QString network, QString sender, QString file);
    void closeDCCFile(QString network, QString sender, QString file);
    void lineCompleted(QString network, QString chat, QString line);
    void showAlert(QString network, QString sender, int type);
    void closeAlert(QString network, QString sender, int type);

private Q_SLOTS:
    void init();
    void startLogin(QString server, QString username, QString password, bool useSsl,
                    bool ignoreSslErrors, int sessionID, int connectionID);
    void startLogout();
    void closeConnection();
    void setColors(QMap<QString, QString> colors);
    void startSession(bool temporary);
    void reconnect();
    void writeLine(QString network, QString chat, QString line);
    void completeLine(QString network, QString chat, QString line, int pos);
    void writeSetting(QString category, QString setting, QString value);
    void setIdle(bool idle, bool alerts, int priority);
    void writeDccFileReply(QString network, QString sender, QString file, bool get);
    void quitNetwork(QString network);
    void closeChat(QString network, QString chat);
    void query(QString network, QString nick);
};

#endif // IRGGUCLIENT_H
