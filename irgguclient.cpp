/****************************************************************************
**  irgguclient.cpp
**
**  Copyright information
**
**      Copyright (C) 2013-2015 Arttu Liimola <arttu.liimola@gmail.com>
**
**  License
**
**      This file is part of IrGGu-Client-Lib.
**
**      This program is free software: you can redistribute it and/or modify
**      it under the terms of the GNU General Public License as published by
**      the Free Software Foundation, either version 3 of the License, or
**      (at your option) any later version.
**
**      This program is distributed in the hope that it will be useful,
**      but WITHOUT ANY WARRANTY; without even the implied warranty of
**      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**      GNU General Public License for more details.
**
**      You should have received a copy of the GNU General Public License
**      along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "irgguclient.h"

/**
 * Constructor.
 *
 * @param *parent Pointer to parent.
 */
IrGGuClient::IrGGuClient (QObject *parent) : QObject(parent)
{
    socket = NULL;

    init();
}

/**
 * Creates connection.
 *
 * @param reconnect Specifies time to wait before reconnecting.
 */
void IrGGuClient::createConnection ()
{
    if ( connection )
    {
        connection->disconnect();
        connection->deleteLater();
    }

    connection = new Connection(socket, login, this);

    connect(connection, SIGNAL(sessionStopped()), this, SLOT(closeConnection()));
    connect(connection, SIGNAL(insertNetwork(QString)), this, SIGNAL(insertNetwork(QString)));
    connect(connection, SIGNAL(insertChat(QString,QString)), this,
            SIGNAL(insertChat(QString,QString)));
    connect(connection, SIGNAL(insertNick(QString,QString,QString)), this,
            SIGNAL(insertNick(QString,QString,QString)));
    connect(connection, SIGNAL(insertNicks(QString,QString,QStringList)), this,
            SIGNAL(insertNicks(QString,QString,QStringList)));
    connect(connection, SIGNAL(removeNetwork(QString)), this, SIGNAL(removeNetwork(QString)));
    connect(connection, SIGNAL(removeChat(QString,QString)), this,
            SIGNAL(removeChat(QString,QString)));
    connect(connection, SIGNAL(removeNick(QString,QString,QString)), this,
            SIGNAL(removeNick(QString,QString,QString)));
    connect(connection, SIGNAL(changeNick(QString,QString,QString,QString)), this,
            SIGNAL(changeNick(QString,QString,QString,QString)));
    connect(connection, SIGNAL(changeNickMode(QString,QString,QString,QString)), this,
            SIGNAL(changeNickMode(QString,QString,QString,QString)));
    connect(connection, SIGNAL(renameChat(QString,QString,QString)), this,
            SIGNAL(renameChat(QString,QString,QString)));
    connect(connection, SIGNAL(newMsg(QString,QString,Line)), this,
            SIGNAL(newMsg(QString,QString,Line)));
    connect(connection, SIGNAL(newHighlightMsg(QString,QString,Line)), this,
            SIGNAL(newHighlightMsg(QString,QString,Line)));
    connect(connection, SIGNAL(newOwnMsg(QString,QString,Line)), this,
            SIGNAL(newOwnMsg(QString,QString,Line)));
    connect(connection, SIGNAL(appendMsg(QString,QString,Line)), this,
            SIGNAL(appendMsg(QString,QString,Line)));
    connect(connection, SIGNAL(newDCCFile(QString,QString,QString)), this,
            SIGNAL(newDCCFile(QString,QString,QString)));
    connect(connection, SIGNAL(closeDCCFile(QString,QString,QString)), this,
            SIGNAL(closeDCCFile(QString,QString,QString)));
    connect(connection, SIGNAL(lineCompleted(QString,QString,QString)), this,
            SIGNAL(lineCompleted(QString,QString,QString)));
    connect(connection, SIGNAL(showAlert(QString,QString,int)), this,
            SIGNAL(showAlert(QString,QString,int)));
    connect(connection, SIGNAL(closeAlert(QString,QString,int)), this,
            SIGNAL(closeAlert(QString,QString,int)));

    connection->connectToServer();
}

/**
 * Initialize.
 */
void IrGGuClient::init ()
{
    if ( socket )
    {
        socket->disconnect();
        socket->deleteLater();
    }

    socket     = new TcpSocket(this);
    login      = NULL;
    connection = NULL;

    connect(socket, SIGNAL(connected()), this, SIGNAL(connected()));
    connect(socket, SIGNAL(disconnected()), this, SIGNAL(disconnected()));
    connect(socket, SIGNAL(unableToConnect()), this, SIGNAL(unableToConnect()));
}

/**
 * Starts login.
 *
 * @param server          Server in format host/port.
 * @param username        Username.
 * @param password        Password.
 * @param useSsl          Specifies if SSL is used.
 * @param ignoreSslErrors Specifies if SSL errors are ignored.
 * @param sessionID       Session ID of existing session.
 * @param connectionID    Connection ID of existing session.
 */
void IrGGuClient::startLogin (QString server, QString username, QString password, bool useSsl,
                            bool ignoreSslErrors, int sessionID, int connectionID)
{
    if ( server.contains("/") )
    {
        QStringList host = server.split('/');

        if ( login )
        {
            login->disconnect();
            login->deleteLater();
        }

        login = new Login(socket, host.at(0), username, password, host.at(1).toInt(), useSsl,
                          ignoreSslErrors, sessionID, connectionID, this);

        connect(login, SIGNAL(resultOfLogin(int,int,int)), this, SIGNAL(loginResult(int,int,int)));

        createConnection();
    }
}

/**
 * Starts logout.
 */
void IrGGuClient::startLogout ()
{
    connection->stopSession();
}

/**
 * Closes connection.
 */
void IrGGuClient::closeConnection ()
{
    connection->disconnect();
    connection->deleteLater();

    login->disconnect();
    login->deleteLater();

    connect(socket, SIGNAL(disconnected()), this, SLOT(init()));

    socket->disconnectFromHost();
}

/**
 * Sets colors to server.
 *
 * @param colors List of colors.
 */
void IrGGuClient::setColors(QMap<QString, QString> colors)
{
    connection->setColors(colors);
}

/**
 * Starts session.
 *
 * @param temporary Specifies if session is temporary.
 */
void IrGGuClient::startSession (bool temporary)
{
    connection->startSession(temporary);
}

/**
 * Reconnects to server.
 */
void IrGGuClient::reconnect ()
{
    connection->connectToServer();
}

/**
 * Writes line to IRC.
 *
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Line to be written.
 */
void IrGGuClient::writeLine (QString network, QString chat, QString line)
{
    connection->writeLine(network, chat, line);
}

/**
 * Completes line.
 *
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Line to be completed.
 * @param pos     Cursor position.
 */
void IrGGuClient::completeLine (QString network, QString chat, QString line, int pos)
{
    connection->completeLine(network, chat, line, pos);
}

/**
 * Writes setting.
 *
 * @param category Setting category.
 * @param setting  Setting name.
 * @param value    Setting value.
 */
void IrGGuClient::writeSetting (QString category, QString setting, QString value)
{
    connection->writeSetting(category, setting, value);
}

/**
 * Sets idle state.
 *
 * @param idle     Specifies idle state.
 * @param alerts   Specifies alerts state.
 * @param priority Priority of client.
 */
void IrGGuClient::setIdle (bool idle, bool alerts, int priority)
{
    connection->setIdle(idle, alerts, priority);
}

/**
 * Writes DCC file reply.
 *
 * @param network Network name.
 * @param sender  Sender's name.
 * @param file    Filename.
 * @param get     Specifies if the file should be accepted.
 */
void IrGGuClient::writeDccFileReply (QString network, QString sender, QString file, bool get)
{
    connection->writeDccFileReply(network, sender, file, get);
}

/**
 * Quits network.
 *
 * @param network Network name.
 */
void IrGGuClient::quitNetwork (QString network)
{
    connection->quitNetwork(network);
}

/**
 * Closes chat.
 *
 * @param network Network name.
 * @param chat    Chat name.
 */
void IrGGuClient::closeChat (QString network, QString chat)
{
    connection->closeChat(network, chat);
}

/**
 * Opens private chat with user.
 *
 * @param network Network name.
 * @param nick    User's nick.
 */
void IrGGuClient::query (QString network, QString nick)
{
    connection->query(network, nick);
}

/**
 * Signal is emitted when client has connected to server.
 *
 * @fn void IrGGuClient::connected()
 */

/**
 * Signal is emitted when client has disconnected from server.
 *
 * @fn void IrGGuClient::disconnected()
 */

/**
 * Signal is emitted when client was unable to connect to server.
 *
 * @fn void IrGGuClient::unableToConnect()
 */

/**
 * Signal is emitted when login result has been received.
 *
 * @fn void IrGGuClient::loginResult(int result, int sessionID, int connectionID)
 * @param result       Login Result.
 * @param sessionID    Session ID.
 * @param connectionID Connection ID.
 */

/**
 * Signal is emitted when insert network command has been received.
 *
 * @fn void IrGGuClient::insertNetwork(QString network)
 * @param network Network name.
 */

/**
 * Signal is emitted when insert chat command has been received.
 *
 * @fn void IrGGuClient::insertChat(QString network, QString chat)
 * @param network Network name.
 * @param chat    Chat name.
 */

/**
 * Signal is emitted when insert nick command has been received.
 *
 * @fn void IrGGuClient::insertNick(QString network, QString chat, QString nick)
 * @param network Network name.
 * @param chat    Chat name.
 * @param nick    Nick.
 */

/**
 * Signal is emitted when insert nicks command has been received.
 *
 * @fn void IrGGuClient::insertNicks(QString network, QString chat, QStringList nicks)
 * @param network Network name.
 * @param chat    Chat name.
 * @param nicks   Nicks.
 */

/**
 * Signal is emitted when remove network command has been received.
 *
 * @fn void IrGGuClient::removeNetwork(QString network)
 * @param network Network name.
 */

/**
 * Signal is emitted when remove chat command has been received.
 *
 * @fn void IrGGuClient::removeChat(QString network, QString chat)
 * @param network Network name.
 * @param chat    Chat name.
 */

/**
 * Signal is emitted when remove nick command has been received.
 *
 * @fn void IrGGuClient::removeNick(QString network, QString chat, QString nick)
 * @param network Network name.
 * @param chat    Chat name.
 * @param nick    Nick.
 */


/**
 * Signal is emitted when change nick command has been received.
 *
 * @fn void IrGGuClient::changeNick(QString network, QString chat, QString oldNick, QString newNick)
 * @param network Network name.
 * @param chat    Chat name.
 * @param oldNick Old nick.
 * @param newNick New nick.
 */

/**
 * Signal is emitted when change nick mode command has been received.
 *
 * @fn void IrGGuClient::changeNickMode(QString network, QString chat, QString nick, QString mode)
 * @param network Network name.
 * @param chat    Chat name.
 * @param nick    Nick.
 * @param mode    New mode.
 */

/**
 * Signal is emitted when rename chat command has been received.
 *
 * @fn void IrGGuClient::renameChat(QString network, QString oldChat, QString newChat)
 * @param network Network name.
 * @param oldChat Old chat name.
 * @param newChat New chat name.
 */

/**
 * Signal is emitted when new message command has been received.
 *
 * @fn void IrGGuClient::newMsg(QString network, QString chat, Line line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Chat line.
 */

/**
 * Signal is emitted when new highlight message command has been received.
 *
 * @fn void IrGGuClient::newHighlightMsg(QString network, QString chat, Line line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Chat line.
 */


/**
 * Signal is emitted when new own message command has been received.
 *
 * @fn void IrGGuClient::newOwnMsg(QString network, QString chat, Line line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Chat line.
 */

/**
 * Signal is emitted when append message command has been received.
 *
 * @fn void IrGGuClient::appendMsg(QString network, QString chat, Line line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Chat line.
 */

/**
 * Signal is emitted when new DCC file command has been received.
 *
 * @fn void IrGGuClient::newDCCFile(QString network, QString sender, QString file)
 * @param network Network name.
 * @param sender  Sender's name.
 * @param file    Filename.
 */

/**
 * Signal is emitted when close DCC file command has been received.
 *
 * @fn void IrGGuClient::closeDCCFile(QString network, QString sender, QString file)
 * @param network Network name.
 * @param sender  Sender's name.
 * @param file    Filename.
 */

/**
 * Signal is emitted when line completed command has been received.
 *
 * @fn void IrGGuClient::lineCompleted(QString network, QString chat, QString line)
 * @param network Network name.
 * @param chat    Chat name.
 * @param line    Completed line.
 */

/**
 * Signal is emitted when alert command has been received.
 *
 * @fn void IrGGuClient::showAlert(QString network, QString sender, int type)
 * @param network Network name.
 * @param sender  Triggerer of alert.
 * @param type    Alert type.
 */

/**
 * Signal is emitted when close alert command has been received.
 *
 * @fn void IrGGuClient::closeAlert(QString network, QString sender, int type)
 * @param network Network name.
 * @param sender  Triggerer of alert.
 * @param type    Alert type.
 */
